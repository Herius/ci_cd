from datetime import datetime
import json
import requests
import random
import numpy as np
from pytz import country_timezones
import schedule
import time

def fetchWeatherAPI(cityName):
    ### you can change cityName below to the city you want get info 
    url = "https://community-open-weather-map.p.rapidapi.com/weather"
    querystring = {"q":cityName, "units":"metric"}
    headers = {
        "X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com",
        "X-RapidAPI-Key": "05c9daee8bmsh169afb2a83ec792p1a959cjsna88fccb5b2b7"
    }

    return requests.request("GET", url, headers=headers, params=querystring).json()

def Insert_City(apiResult, db):
    row = ( apiResult['id'], 
            apiResult['id'], 
            apiResult['name'], 
            apiResult['coord']['lat'], 
            apiResult['coord']['lon'] )
    query = '''
        USE WeatherAPI
        IF NOT EXISTS (SELECT * FROM City WHERE CityId = ?)
            INSERT INTO City VALUES (?, ?, ?, ?)'''
    
    db.execute(query, row)

def Insert_Station(apiResult, db):
    timezone = country_timezones[apiResult['sys']['country']] # get time zone by country name

    row = ( apiResult['sys']['id'], 
            apiResult['sys']['id'], 
            apiResult['sys']['type'], 
            apiResult['sys']['country'], 
            timezone[0] )
    query = '''
        USE WeatherAPI
        IF NOT EXISTS (SELECT * FROM Station WHERE StationId = ?)
            INSERT INTO Station VALUES (?, ?, ?, ?)'''
    
    db.execute(query, row)

def Insert_WeatherCondition(apiResult, db):
    row = ( apiResult['weather'][0]['id'],
            apiResult['weather'][0]['id'], 
            apiResult['weather'][0]['main'], 
            apiResult['weather'][0]['description'], 
            apiResult['weather'][0]['icon'] )
    query = '''
        USE WeatherAPI
        IF NOT EXISTS (SELECT * FROM WeatherCondition WHERE ConditionId = ?)
            INSERT INTO WeatherCondition VALUES (?, ?, ?, ?)'''
    
    db.execute(query, row)

# convert datetime to seconds
def dt2sec(dt):
    return dt.hour*3600 + dt.minute*60 + dt.second

def unix2dt(unix_cod, tz=None):
    if tz:
        return datetime.utcfromtimestamp(int(unix_cod + tz)) # convert unix string to datetime in that timezone
    return datetime.utcfromtimestamp(int(unix_cod + 25200)) # convert unix string to datetime in UTC+7
        
def Insert_Record(apiResult, db):
    datetime_query = unix2dt(apiResult['dt'])
    sunrise = unix2dt(apiResult['sys']['sunrise'], apiResult['timezone'])
    sunset = unix2dt(apiResult['sys']['sunset'], apiResult['timezone'])

    sunrise2second = dt2sec(sunrise)
    sunset2second = dt2sec(sunset)

    rainvolume = snowvolume = sea_level = grnd_level = wind_gust= 0

    if 'sea_level' in apiResult['main']:
        sea_level = apiResult['main']['sea_level']
    if 'grnd_level' in apiResult['main']:
        grnd_level = apiResult['main']['grnd_level']
    if 'gust' in apiResult['wind']:
        wind_gust = apiResult['wind']['gust']
    if 'rain' in apiResult:
        rainvolume = apiResult['rain']['1h']
    if 'snow' in apiResult:
        snowvolume = apiResult['snow']['1h']

    row = ( apiResult['main']['temp'],
            apiResult['main']['temp_min'], 
            apiResult['main']['temp_max'], 
            apiResult['main']['pressure'], 
            apiResult['main']['humidity'], 
            sea_level, 
            grnd_level,
            apiResult['visibility'],
            apiResult['wind']['speed'],
            apiResult['wind']['deg'],
            wind_gust,
            apiResult['clouds']['all'],
            rainvolume,
            snowvolume,
            sunrise2second,
            sunset2second,
            datetime_query,
            apiResult['weather'][0]['id'],
            apiResult['id'],
            apiResult['sys']['id'] )
    query = '''
        USE WeatherAPI
        INSERT INTO Record VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    
    db.execute(query, row)

def API():
    print('Calling API')
    ### this will random choose one of city name in list
    cityList = np.array(["Hanoi", "Seoul", "Tokyo", "Bangkok", "California", "Saigon", "London", "Paris"])
    cityName = cityList[random.randint(0,len(cityList)-1)]
    apiResult = fetchWeatherAPI(cityName)
    if apiResult['cod'] == 200:
        Insert_City(apiResult, conn.db)
        Insert_Station(apiResult, conn.db)
        Insert_WeatherCondition(apiResult, conn.db)
        Insert_Record(apiResult, conn.db)
        print('Done')
    else:
        print(apiResult['message'])

    ### Change option to 1 if you want to write fetch data to json file.
    ### But the json will be replaced by next fetch
    ### Please change your file name below befor fetch if want to keep the last data (default: data.json)
    option = 0
    if option:
        with open('data.json', 'w') as f:
            json.dump(apiResult, f, indent=4)

    print('Sleeping\n-----------------\n\n')

if __name__ == '__main__':
    import conn
    # schedule to run every 30 seconds
    schedule.every(30).seconds.do(API)
    while True:
        schedule.run_pending()
        time.sleep(1)
